#ifndef __random_h__
#define __random_h__

#include <math.h> 
#include <stdio.h>
#define MODULE  2147483647
#define A       16807
#define LASTXN  127773
#define UPTOMOD -2836
#define RATIO   0.46566128e-9            /* 1/MODULE */

long rnd32(long seed);
long double uniform(long double a, long double b, long *seed);
long double negexp (long double mean, long *seed);
long poisson(long double alpha,long *seed);
int geometric0(double mean,long *seed);

#endif