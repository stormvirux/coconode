#============================================================
# GnuPlot script : CONFIG.GNU
#============================================================
# Parameter 0 : input data file
# Parameter 1 : graph title
# Parameter 2 : min X
# Parameter 3 : max X
# Parameter 4 : min Y
# Parameter 5 : max Y
# Parameter 6 : output file (PNG format)
#============================================================

# Setting
set xlabel "Wavelength (A)"
set ylabel "Relative intensity"
set title '$1' 0,-0.5
set tmargin 2
set xrange [$2:$3]
set yrange [$4:$5]
set terminal push

# Plot on the computer screen
set terminal windows "Arial" 9
plot "$0" notitle with lines lc rgb 'black'

# Plot on a PNG file
set terminal png small size 900,500
set output '$6'
replot

# Finalize
set output
set terminal pop
reset