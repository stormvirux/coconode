#include <stdlib.h>
#include <gtk/gtk.h>

int main(int argc, char *argv []) 
{ 
	GtkWidget *fenetre_principale = NULL; 

	GtkBuilder *builder = NULL; 
	GError *error = NULL; 
	gchar *filename = NULL; 
	/* Initialisation de la librairie Gtk. */ 
	gtk_init(&argc, &argv); 

	/* Ouverture du fichier Glade de la fenêtre principale */ 
	builder = gtk_builder_new(); 
	
	/* Création du chemin complet pour accéder au fichier gui.glade. */ 
	/* g_build_filename(); construit le chemin complet en fonction du système */ 
	/* d'exploitation. ( / pour Linux et \ pour Windows) */ 
	filename =  g_build_filename ("gui.glade", NULL); 

		/* Chargement du fichier gui.glade. */ 
	gtk_builder_add_from_file (builder, filename, &error); 
	g_free (filename); 
	if (error) 
	{ 
		gint code = error->code; 
		g_printerr("%s\n", error->message); 
		g_error_free (error); 
		return code; 
	} 

		/* Récupération du pointeur de la fenêtre principale */ 
	fenetre_principale = GTK_WIDGET(gtk_builder_get_object (builder, "MainWindow")); 
	
	const gchar *icon = "./../media/coconut-icon.png\0";
	gtk_window_set_icon_from_file(GTK_WINDOW(fenetre_principale), icon, NULL);

	/* Affectation du signal "destroy" à la fonction gtk_main_quit(); pour la */ 
	/* fermeture de la fenêtre. */ 
	g_signal_connect (G_OBJECT (fenetre_principale), "destroy", (GCallback)gtk_main_quit, NULL); 

	/* Affichage de la fenêtre principale. */ 
	gtk_widget_show_all (fenetre_principale); 

	gtk_main(); 

	return EXIT_SUCCESS; 
} 
	
