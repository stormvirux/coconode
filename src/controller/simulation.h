#ifndef __SIMULATION_H__
#define __SIMULATION_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <jni.h>

int simulation (char *csc_config_file, char *contiki_path) ;
int stopping_simulation ();
int pausing_simulation ();
int restart_simulation ();
int check_simulation ();
long time_simulation();
int init_jvm();
#endif