import java.net.URL;
import java.net.URI;
import java.net.URLClassLoader;
import java.lang.reflect.Method;
import java.io.File;


public class Loader {

    public Loader(){
    }
    
    public void addURL(String name){
	URLClassLoader systemClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
	try{
	  URL url = new File(name).toURI().toURL();
	  Method addURL = URLClassLoader.class.getDeclaredMethod("addURL", new Class<?>[]{URL.class}); 
	  addURL.setAccessible(true);
	  addURL.invoke(systemClassLoader, new Object[]{url});
	}
	catch(Exception e){
	  e.printStackTrace();
	}
    
    }
}