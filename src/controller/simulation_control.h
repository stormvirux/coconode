#ifndef __SIMULATION_CTRL_H__
#define __SIMULATION_CTRL_H__
  
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <jni.h>
#include "../gui/main_window.h"
#include "simulation.h"



typedef enum {
	START_S = 1,
	PAUSE_S = 2,
	RESTART_S = 3,
	STOP_S = 4
} Command;

typedef struct {
  Command cmd;
} message;

// void run_simulation (gpointer data);
// gpointer thread_simulation (gpointer data);
// void start_simulation_message() ;
// void stop_simulation_message();
// void send_message(message *m);
// message *new_message(Command cmd);
// void init();
int cp(const char *from_name, const char *to_name);
int length(int n);
char* last_element(char *repo,char* delimit);
void start_simulation(gpointer data);
void pause_simulation(gpointer data);
void stop_simulation(gpointer data);
void restart_ctrl_simulation(gpointer data);
gboolean on_timer_expired(gpointer user_data);
gboolean on_dad_said(GIOChannel *source,GIOCondition condition,gpointer data);
gpointer thread_scheduler (gpointer data);
void init_jvm_for_simulation();

#endif