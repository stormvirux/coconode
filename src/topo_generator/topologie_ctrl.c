#include "topologie_ctrl.h"


struct node *node;
struct point *t;
struct data_thread *dt;

void *lib_handle;
void *(*ptrfunc)(int number_of_sensors, int percentage_sensor, 
		int percentage_actuator);

void *(*ptrfunc_get_node_label) (int type);
void *(*ptrfunc_get_node_label_length) (int type);

extern const char* name;
// int compilation (GSList *file)
// {
//   
//   
// }
// 
// 
// char *give_namefile(char* file){
//   
// }


gpointer thread_generate_topologie (gpointer gene_data)
{
	void* list;
	Generation_infos *g_data = (Generation_infos *) gene_data;
// 	printf("%d \n",(strlen(g_data->repository)+strlen(name)));
	char *output_data_file = malloc ((strlen(g_data->repository)+strlen(name) + 1)*sizeof(char));
	char* compilation = malloc(1000*sizeof(char));
	char * file=malloc(1000*sizeof(char));
	double x_max = -DBL_MAX;
	double x_min = DBL_MAX;
	double y_min = DBL_MAX;
	double y_max = -DBL_MAX;
// 	printf("MIN MAX : %f %f %f %f\n",x_max,x_min,y_max,y_min);
	int i = 0;
	FILE * topo_file = NULL;
	char * nfout = (char *) malloc(sizeof(char)); 
	char * nameFile = (char *) malloc(40*sizeof(char)); 
	char* libr = malloc(1000*sizeof(char));
	char* node_label;
	int node_label_length;


//  	printf("%f %f %f \n",g_data->nb_nodes, g_data->percentage_sensors, 
//  		      g_data->percentage_actuators);
	if(g_data->new_files){
	    strcpy(compilation,"");
	    GSList *current;
	    current = g_data->files;
	    
    // 	    for(i=0;i<(lgfile-1);i++){
    // 	      *(nfout+i)=nfin[i];
    // 	    }
    // 	    strcat(nfout,"\0");
	    
	    while(current != NULL){
		strcat(file,g_file_get_parse_name((GFile*)current->data));
		strcat(file," ");
    
//      	    printf("%s %s %s\n",g_file_get_parse_name(g_file_get_parent((GFile*)current->data)),g_file_get_basename((GFile*)current->data),g_file_get_parse_name((GFile*)current->data));
		strcat(compilation,"gcc -fPIC -c ");
		strcat(compilation,g_file_get_path((GFile*)current->data));
		strcat(compilation," -o ");
		strcat(compilation,g_file_get_parse_name(g_file_get_parent((GFile*)current->data)));
		nameFile = g_file_get_basename((GFile*)current->data);
    //  	    printf("%d \n",(strlen(nameFile)-1));
		free(nfout);
		nfout = (char *) malloc((strlen(nameFile))*sizeof(char)); 
		for(i=0;i<(strlen(nameFile)-1);i++){
		  *(nfout+i)=nameFile[i];
		}
		*(nfout+i) = '\0';
		strcat(compilation,"/");
		strcat(compilation,nfout);
		strcat(compilation,"o;");
    // 	     printf("%s \n",nfout);
		current = current->next;
	    }
	    system(compilation);
// 	    printf("%s \n",compilation);
	    
	    #ifdef __linux__
		  /* pour creer la commande systeme ("gcc -c chemin/test_couplage.c;ld -shared -o test_couplage.so test_couplage.o ") ; */ 
	    current = g_data->files;
	    
    // 	    for(i=0;i<(lgfile-1);i++){
    // 	      *(nfout+i)=nfin[i];
    // 	    }
    // 	    strcat(nfout,"\0");
	    strcpy(libr,"");
	    strcat(libr,"gcc -shared -o /tmp/user.so ");
	    while(current != NULL){
		strcat(libr,g_file_get_parse_name(g_file_get_parent((GFile*)current->data)));
		nameFile = g_file_get_basename((GFile*)current->data);
    //  	    printf("%d \n",(strlen(nameFile)-1));
		free(nfout);
		nfout = (char *) malloc((strlen(nameFile))*sizeof(char)); 
		for(i=0;i<(strlen(nameFile)-1);i++){
		  *(nfout+i)=nameFile[i];
		}
		*(nfout+i) = '\0';
		strcat(libr,"/");
		strcat(libr,nfout);
		strcat(libr,"o ");
    	    
		current = current->next;
	    }
	    strcat(libr,";");
// 	    printf("%s \n",libr);
	    system(libr);
	    #endif
	    
	}
	lib_handle = dlopen ("/tmp/user.so", RTLD_LAZY) ;
	if (lib_handle){
		printf("Dynamic library opening: Successful\n");	
	} else {
		printf ("[%s] Unable to open library: %s \n", "user.so", dlerror ()) ;
		exit (-1) ;
	}
	  
	ptrfunc = (void *) dlsym (lib_handle, "generate_nodes") ;
	ptrfunc_get_node_label = (void *) dlsym (lib_handle, "get_node_label") ;
	ptrfunc_get_node_label_length = (void *) dlsym (lib_handle, "get_node_label_length") ;

	if (ptrfunc == NULL){
		printf("Loading generate_nodes(): UnSuccessful, default function use\n");
	} else{
		printf("Loading generate_nodes(): Successful\n");
	}

	if (ptrfunc_get_node_label == NULL){
		printf("Cannot load ptrfunc_get_node_label\n");
	} else{
		printf("Loading ptrfunc_get_node_label : Successful\n");
	}

	if (ptrfunc_get_node_label_length == NULL){
		printf("Cannot load ptrfunc_get_node_label_length\n");
	} else{
		printf("Loading ptrfunc_get_node_label_length : Successful\n");
	}

	list = (*ptrfunc)(g_data->nb_nodes, g_data->percentage_sensors, 
		      g_data->percentage_actuators);

	g_data->nb_nodes = das_getsize(list);

	// t = malloc((g_data->nb_nodes+1) * sizeof(struct point));
	t = malloc((g_data->nb_nodes) * sizeof(struct point));

	if(list != NULL){
	   memcpy(output_data_file,g_data->repository,strlen(g_data->repository)*sizeof(char));
	   memcpy((output_data_file+strlen(g_data->repository)),name,(strlen(name)+1)*sizeof(char));

	   // printf("%s \n",output_data_file);
	   topo_file = fopen (output_data_file,"w");
	   if(topo_file == NULL){
	     g_idle_add((GSourceFunc) callback_generate,NULL);
	     return NULL;
	   }
	   fprintf(topo_file,"# format: SIMPLE");
	   i = 0;
	    while ((node = (struct node *) das_traverse(list)) != NULL) {
			if(x_max < node->data.x) 
			{
			  x_max = node->data.x;
			}
			if (x_min > node->data.x)
			{
			  x_min = node->data.x;
			}
			if(y_max < node->data.y)
			{
			  y_max = node->data.y;
			}
			if (y_min > node->data.y)
			{
			  y_min = node->data.y;
			}
			t[i].id = node->data.id;
			t[i].x = node->data.x;
			t[i].y = node->data.y;
			node_label = (char*) (*ptrfunc_get_node_label)(node->data.type);
			node_label_length = (int) (*ptrfunc_get_node_label_length)(node->data.type);
			t[i].type = malloc((node_label_length)*sizeof(char));
			memcpy (t[i].type, node_label, node_label_length* sizeof(char));
			printf("Node = %ld coord = (%lf, %lf) %s\n", 
				node->data.id, node->data.x, node->data.y, node_label);
			fprintf(topo_file,"\n%ld %lf %lf %lf %s", node->data.id, 
				node->data.x, node->data.y, node->data.z, node_label);

		      i++;
	    }
 	    fclose(topo_file);
	    x_max = x_max - x_min;
	    y_max = y_max - y_min;
	    for(i=0;i<g_data->nb_nodes;i++){
	      t[i].x = (t[i].x - x_min) / x_max;
	      t[i].y = (t[i].y - y_min) / y_max;
	    }
	    dt = malloc(sizeof(struct data_thread));
	    // dt->data = malloc((g_data->nb_nodes+1) * sizeof(struct point));
	    // memcpy (dt->data,t,(g_data->nb_nodes+1)* sizeof(struct point));   
	    dt->data = malloc((g_data->nb_nodes) * sizeof(struct point));
	    memcpy (dt->data,t,(g_data->nb_nodes)* sizeof(struct point)); 
	    // for(i=0;i<g_data->nb_nodes;i++){
	   	// 	struct point * points = (struct point *) dt->data;
	      // // memcpy( (points+i)->type, t[i].type, (strlen(t[i].type)+1) * sizeof(char));
	   		// strcat((points+i)->type, t[i].type);
	    // }

	    dt->length = g_data->nb_nodes;
	    

	    g_free(g_data->repository);
	    free(g_data);
	    dlclose(lib_handle);
	    while ((node = (struct node *) das_traverse(list)) != NULL) {
		das_destroy(node->neighbors);
	    }
	    das_destroy(list);
	    g_idle_add((GSourceFunc) callback_generate,dt);

	    //free de t : 
	    // for(i=0;i<g_data->nb_nodes;i++){
	    //   free(t[i].type);
	    // }
	    free(t);
	    
	}
	else{
	  
	}

	free(output_data_file);
	free(file);
	free(compilation);
    free(libr);
    free(nfout);
    free(nameFile);

    return NULL;
}
