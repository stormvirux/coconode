#ifndef __GEN_H__
#define __GEN_H__
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <stdint.h>
#include "das.h"
#include "mem_fs.h"
#include "smarthomegraph_parameters.h"
#include "random.h"

#define CONTROLLER 1
#define SENSOR 2
#define ACTUATOR 3
#define SWITCH 4

#define CONTROLLER_LABEL "controller"
#define SENSOR_LABEL "sensor"
#define ACTUATOR_LABEL "actuator"
#define SWITCH_LABEL "wallswitch"

// label size of each type of node, 
// e.g "sensor" has 6 caracters, plus '\0' at the end = 7
#define CONTROLLER_LABEL_LENGTH 11
#define SENSOR_LABEL_LENGTH 7
#define ACTUATOR_LABEL_LENGTH 9
#define SWITCH_LABEL_LENGTH 11

typedef signed long int uint_t;

struct neighbor {
    uint_t id;
    double x;
    double y;
    double z;
    int type;
};

struct node {
	struct neighbor data;
	void* neighbors;
};
void init_home_plan();
void destroy_rooms();
void init_counters();
void place_sensors();
void place_actuators();
void place_switches();

void place_controller();

struct room {
	unsigned int type;
	unsigned int id;

	// Dimensions in meters
	double x_length;
	double y_length;

	// Sensor nodes counters
	long  int sensor_counter;
	long  int actuator_counter;
	long  int switch_counter;

	// Coordinates of the lower left corner
	double x_coordinate;
	double y_coordinate;
	double z_coordinate;

	// Density coefficient [-3,3] with mean 0. 0 is the typical density.
	unsigned int density_coeff;
};
void place_device(int, struct room *);
long *seed;

int create_Node(long unsigned int id, int type, float x, float y, float z);
void *generate_nodes (int number_of_sensors, int percentage_sensors, 
		int percentage_actuators);
void destroy ();
void init_home_plan();
void init_counters();
void destroy_rooms();
void init_density(struct room *room);
void place_device(int type, struct room *room);
void place_sensors();
void place_actuators();
void place_switches();
void place_controller();

#endif