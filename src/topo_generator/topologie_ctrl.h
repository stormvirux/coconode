#ifndef __TOPOLOGIE_H__
#define __TOPOLOGIE_H__

#include <stdio.h>
#include <gtk/gtk.h>
#include <float.h>
#include <dlfcn.h>
#include <string.h>

#include "../gui/main_window.h"
#include "das.h" 

typedef signed long int uint_t;

struct neighbor {
    uint_t id;
    double x;
    double y;
    double z;
    int type;
};

struct node {
	struct neighbor data;
	void* neighbors;
};

gpointer thread_generate_topologie (gpointer data);
#endif