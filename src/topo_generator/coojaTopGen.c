/*
 *  Cooja topology generator
 *   
 *
 *  Created by Malisha Vucinic 26/04/12
 *  Compile: gcc -o coojaTopGen coojaTopGen.c das.c mem_fs.c
 *  Execute: ./coojaTopGen -i [input graph] -o [output file]
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <stdint.h>
#include "das.h"
#include "mem_fs.h"
#include "coojaTopGen_parameters.h"

#define LONG 100

FILE * inputFile;
FILE * outputFile;

// char input_file[100];
// char output_file[100];

// char* usage_str="Error: Entry should be in the following form: -i [input_file] -o [output_file] -n [number of wallswitches] -t [simulation time in ms]'\n'";

// void usage(void){
// 	fprintf(stderr, "%s", usage_str);
// 	return;
// }

struct mote {

	float x_coordinate;
	float y_coordinate;
	float z_coordinate;

	char type[30];	
	int	id;
};

struct mote_type {
	char moteType[50];
	char identifier[30];
	char description[50];
	char source_path[150];
	char commands[150];
	char firmware_path[150];
	char device_type[30];
};

void printXmlVersion(char *,char *);
void printSimconfHeader(); 
void printProjectExport();
void printPlugin(long unsigned);
void printSimulationParameters(char *title, int delaytime, long int randomseed, int motedelay, char *radiomedium, float transmission_range, float interference_range, float success_ratio_tx, float success_ratio_rx, long int logoutput);

void printMoteType(char *moteType, char *identifier, char *description, char *source_path, char *commands, char *firmware_path);

void printMote(double x_coordinate, double y_coordinate, double z_coordinate, int id, char * motetype_identifier);
void createMoteType(char *device_type);
void printSimulation();

void * list_of_mote_types;
struct mote_type *mote_type;

int number_of_mote_types = 0;
int mote_counter = 0;
int number_of_wallswitches = 0;
unsigned long simulation_time = 0;

char wallswitch_command[100];

int generate_csc_file(char *input_file, char *output_file, int simulation_time, char *contiki_repo){
	int flag = 0;
	struct mote temp_mote;
	long int randomseed = time(NULL);
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	number_of_wallswitches = 0;
				
	if(input_file != NULL && output_file != NULL){
				
		if((outputFile = fopen (output_file,"w")) == NULL)
		{		
			printf("Cannot open the output file for writing! : %s\n", output_file); exit(0);
		}
		
		if((inputFile = fopen (input_file, "r")) == NULL)
		{
			printf("Cannot open the input file for reading! : %s\n", input_file); exit(0);
		}
		
		while ((read = getline(&line, &len, inputFile)) != -1) {
		      if (strstr (line, "wallswitch") != NULL) {
		      number_of_wallswitches = number_of_wallswitches + 1;
		      }
		}
		rewind(inputFile);
		// ***
		// Checking the format of the file specified on the first line. Only supporting SIMPLE!
		fscanf(inputFile, "%s %s %s",temp_mote.type, temp_mote.type, temp_mote.type);
		if(strcmp(temp_mote.type, "SIMPLE"))
		{
		  printf("ERROR: Wrong format of the input file.\n");
		  exit(0);
		}

		// If format is OK, proceed with writing the output file
		das_init();
		list_of_mote_types = das_create();
		printXmlVersion(VERSION, ENCODING);	
		printSimconfHeader(1);
		printProjectExport();
		printSimulation(1);
		
		printSimulationParameters(TITLE, DELAY_TIME, randomseed, MOTE_DELAY, RADIOMEDIUM, TRANSMISSION_RANGE, INTERFERENCE_RANGE, SUCCESS_RATIO_TX, SUCCESS_RATIO_RX, LOGOUTPUT);

		// ***

		// Scanning the file line by line.
		while(fscanf(inputFile, "%d %f %f %f %s", &temp_mote.id, &temp_mote.x_coordinate, &temp_mote.y_coordinate, &temp_mote.z_coordinate, temp_mote.type) != EOF)
		{
			while ((mote_type = (struct mote_type *) das_traverse(list_of_mote_types)) != NULL)
			{
				if(strcmp(mote_type->device_type, temp_mote.type) == 0)
				{
					flag = 1;
					//break;
				}
				
			}

			if(flag == 0)
			{
				createMoteType(temp_mote.type);
				printMoteType(mote_type->moteType, mote_type->identifier, mote_type->description, mote_type->source_path, mote_type->commands, mote_type->firmware_path);
			}
			else
			{
				flag = 0;
			}
		}

		rewind(inputFile);
	
		// Discard the first line, again.
		fscanf(inputFile, "%s %s %s",temp_mote.type, temp_mote.type, temp_mote.type);
		// ///
		
		// Go line by line for each node in the topology and generate <mote> headers in the cooja file
		while(fscanf(inputFile, "%d %f %f %f %s", &temp_mote.id, &temp_mote.x_coordinate, &temp_mote.y_coordinate, &temp_mote.z_coordinate, temp_mote.type) != EOF)
		{
			while ((mote_type = (struct mote_type *) das_traverse(list_of_mote_types)) != NULL)
			{
				if(strcmp(mote_type->device_type, temp_mote.type) == 0)
				{		
				  printMote(temp_mote.x_coordinate, (-1) * temp_mote.y_coordinate, temp_mote.z_coordinate, ++mote_counter, mote_type->identifier);
				}
				
			}


		}
		// ***

		printSimulation(0);
		printPlugin(simulation_time * 60 * 1000); // convert to ms
		printSimconfHeader(0);
			

				//Close file
		fclose (inputFile);
		fclose (outputFile);
	}
	else {
		// usage();
		exit(0);
	}

	das_destroy(list_of_mote_types);
	
	return 0; 
}

void createMoteType(char *device_type)
{
	char temp[150];
	
	mote_type = (struct mote_type *) malloc(sizeof(struct mote_type));
	strcpy(mote_type->device_type, device_type);
	//printf("type is: %s\n", mote_type->device_type);
	
/*
	char moteType[50];
	char identifier[30];
	char description[50];
	char source_path[150];
	char commands[150];
	char firmware_path[150];
	char device_type[30];
*/			
	// printf("%s \n",mote_type->device_type);
	if(strcmp(mote_type->device_type, "sensor") == 0)
	{
		strcpy(mote_type->moteType, SKYMOTE_STRING);
	
		sprintf(temp,"sky%d",++number_of_mote_types);	
		strcpy(mote_type->identifier, temp);

		strcpy(mote_type->description, "Sensor node");
		strcpy(mote_type->source_path, SENSOR_SOURCE_PATH);
		strcpy(mote_type->commands, SENSOR_COMMANDS);
		strcpy(mote_type->firmware_path, SENSOR_FIRMWARE_PATH);
		
		
	}
	else if(strcmp(mote_type->device_type, "actuator") == 0)
	{
	/*
	* NEEDS TO BE CHANGED !!!
	*/

		strcpy(mote_type->moteType, SKYMOTE_STRING);
	
		sprintf(temp,"sky%d",++number_of_mote_types);	
		strcpy(mote_type->identifier, temp);

		strcpy(mote_type->description, "Actuator node");
		strcpy(mote_type->source_path, ACTUATOR_SOURCE_PATH);
		strcpy(mote_type->commands, ACTUATOR_COMMANDS);
		strcpy(mote_type->firmware_path, ACTUATOR_FIRMWARE_PATH);

	}
	else if(strcmp(mote_type->device_type, "controller") == 0)
	{
	/*
	*  Take care of the path in macros!
	*/

		strcpy(mote_type->moteType, SKYMOTE_STRING);
	
		sprintf(temp,"sky%d",++number_of_mote_types);	
		strcpy(mote_type->identifier, temp);

		strcpy(mote_type->description, "Main Controler Node");
		strcpy(mote_type->source_path, CONTROLER_SOURCE_PATH);
		strcpy(mote_type->commands, CONTROLER_COMMANDS);
		strcpy(mote_type->firmware_path, CONTROLER_FIRMWARE_PATH);

	}
	else if(strcmp(mote_type->device_type, "wallswitch") == 0)
	{
	/*
	* NEEDS TO BE CHANGED !!!
	*/

		sprintf(wallswitch_command, "make udp_wallswitch.sky TARGET=sky DEFINES=NUMBER_OF_WALLSWITCHES=%d",number_of_wallswitches); 

		strcpy(mote_type->moteType, SKYMOTE_STRING);
	
		sprintf(temp,"sky%d",++number_of_mote_types);	
		strcpy(mote_type->identifier, temp);

		strcpy(mote_type->description, "Wall switch node");
		strcpy(mote_type->source_path, WALLSWITCH_SOURCE_PATH);
		strcpy(mote_type->commands, wallswitch_command);
		strcpy(mote_type->firmware_path, WALLSWITCH_FIRMWARE_PATH);

	}
	else
	{
		printf("Device type found in the file not supported.\n");
		exit(0);
	}
	
	das_insert(list_of_mote_types, (void *) mote_type);
}


void printXmlVersion(char *version, char* encoding)
{
	fprintf(outputFile, "<?xml version=\"%s\" encoding=\"%s\"?>\n",version, encoding);
}

void printSimconfHeader(int opening)
{
	if(opening)
	{
	fprintf(outputFile, "<simconf>\n");
	}
	else
	{
	fprintf(outputFile, "</simconf>\n");
	}
}

void printProjectExport()
{
fprintf(outputFile, "<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mrm</project>\n");
fprintf(outputFile, "<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mspsim</project>\n");
fprintf(outputFile, "<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/avrora</project>\n");
fprintf(outputFile, "<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/serial_socket</project>\n");
fprintf(outputFile, "<project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/collect-view</project>\n");
}

void printSimulation(int opening)
{
if(opening)
fprintf(outputFile, "<simulation>\n");
else
fprintf(outputFile,"</simulation>\n");

}

void printSimulationParameters(char *title, int delaytime, long int randomseed, int motedelay, char *radiomedium, float transmission_range, float interference_range, float success_ratio_tx, float success_ratio_rx, long int logoutput)
{
	fprintf(outputFile, "<title>%s</title>\n", title);
	fprintf(outputFile, "<delaytime>%d</delaytime>\n", delaytime);
	fprintf(outputFile, "<randomseed>%ld</randomseed>\n", randomseed);
	fprintf(outputFile, "<motedelay_us>%d</motedelay_us>\n", motedelay);
	fprintf(outputFile, "<radiomedium>\n");
	fprintf(outputFile, "se.sics.cooja.radiomediums.%s\n", radiomedium);
	fprintf(outputFile, "<transmitting_range>%.1f</transmitting_range>\n",transmission_range);
	fprintf(outputFile, "<interference_range>%.1f</interference_range>\n",interference_range);
	fprintf(outputFile, "<success_ratio_tx>%.2f</success_ratio_tx>\n", success_ratio_tx);
	fprintf(outputFile, "<success_ratio_rx>%.2f</success_ratio_rx>\n", success_ratio_rx);
	fprintf(outputFile, "</radiomedium>\n");
	fprintf(outputFile, "<events>\n");
	fprintf(outputFile, "<logoutput>%ld</logoutput>\n", logoutput);
	fprintf(outputFile, "</events>\n");
}

void printMoteType(char *moteType, char *identifier, char *description, char *source_path, char *commands, char *firmware_path)
{
	fprintf(outputFile, "<motetype>\n");
	fprintf(outputFile, "se.sics.cooja.%s\n", moteType);
	fprintf(outputFile, "<identifier>%s</identifier>\n", identifier);
	fprintf(outputFile, "<description>%s</description>\n", description);
	fprintf(outputFile, "<source EXPORT=\"discard\">[CONTIKI_DIR]/%s</source>\n",source_path);
	fprintf(outputFile, "<commands EXPORT=\"discard\">%s</commands>\n",commands);
	fprintf(outputFile, "<firmware EXPORT=\"copy\">[CONTIKI_DIR]/%s</firmware>\n",firmware_path);
	
	fprintf(outputFile, "<moteinterface>se.sics.cooja.interfaces.Position</moteinterface>\n\
	    <moteinterface>se.sics.cooja.interfaces.RimeAddress</moteinterface>\n\
	    <moteinterface>se.sics.cooja.interfaces.IPAddress</moteinterface>\n\
	    <moteinterface>se.sics.cooja.interfaces.Mote2MoteRelations</moteinterface>\n\
	    <moteinterface>se.sics.cooja.interfaces.MoteAttributes</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.MspClock</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.MspMoteID</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.SkyButton</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.SkyFlash</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.SkyCoffeeFilesystem</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.SkyByteRadio</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.MspSerial</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.SkyLED</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.MspDebugOutput</moteinterface>\n\
	    <moteinterface>se.sics.cooja.mspmote.interfaces.SkyTemperature</moteinterface>\n");

	fprintf(outputFile, "</motetype>\n");

}

void printMote(double x_coordinate, double y_coordinate, double z_coordinate, int id, char * motetype_identifier)
{
	fprintf(outputFile, "<mote>\n");
	fprintf(outputFile, "<breakpoints />\n");
	fprintf(outputFile, "<interface_config>\n");
	fprintf(outputFile, "se.sics.cooja.interfaces.Position\n");
	fprintf(outputFile, "<x>%.2f</x>\n",x_coordinate);
	fprintf(outputFile, "<y>%.2f</y>\n", y_coordinate);
	fprintf(outputFile, "<z>%.2f</z>\n", z_coordinate);
	fprintf(outputFile, "</interface_config>\n");
	fprintf(outputFile, "<interface_config>\n");
	fprintf(outputFile, "se.sics.cooja.mspmote.interfaces.MspMoteID\n");
	fprintf(outputFile, "<id>%d</id>\n", id);
	fprintf(outputFile, "</interface_config>\n");
	fprintf(outputFile, "<motetype_identifier>%s</motetype_identifier>\n",motetype_identifier);
	fprintf(outputFile, "</mote>\n");


}

void printScript(long unsigned timeout)
{
	FILE *scriptFile;
	char line[150];

	if((scriptFile = fopen("./src/topo_generator/javascript_for_cooja.txt","r")) == NULL)
	{		
		printf("Cannot open javascript_for_cooja.txt \n"); exit(0);
	}
	while(!feof(scriptFile))
	{
		if(fgets(line, 148, scriptFile))
		{
			if(strncmp(line,"TIMEOUT", 7) == 0)
			{
				fprintf(outputFile, "TIMEOUT(%lu);&#xD;\n", timeout);
			}
			else
			{
				fprintf(outputFile, "%s", line);
			}
		}	
	}

	fclose(scriptFile);	
}

void printPlugin(long unsigned timeout)
{

fprintf(outputFile, "<plugin>\nse.sics.cooja.plugins.SimControl\n\
      <width>318</width>\n\
      <z>2</z>\n\
      <height>192</height>\n\
      <location_x>0</location_x>\n\
      <location_y>0</location_y>\n\
      </plugin>\n\
      <plugin>\n\
      se.sics.cooja.plugins.Visualizer\n\
      <plugin_config>\n\
      <skin>se.sics.cooja.plugins.skins.IDVisualizerSkin</skin>\n\
      <skin>se.sics.cooja.plugins.skins.MoteTypeVisualizerSkin</skin>\n\
      <skin>se.sics.cooja.plugins.skins.UDGMVisualizerSkin</skin>\n\
      <viewport>2.342139199538282 0.0 0.0 2.342139199538282 38.77257217479485 3.566631970609916</viewport>\n\
      </plugin_config>\n\
      <width>300</width>\n\
      <z>3</z>\n\
      <height>300</height>\n\
      <location_x>1380</location_x>\n\
      <location_y>0</location_y>\n\
      </plugin>\n\
      <plugin>\n\
      se.sics.cooja.plugins.LogListener\n\
      <plugin_config>\n\
      <filter />\n\
      <coloring />\n\
      </plugin_config>\n\
      <width>1680</width>\n\
      <z>1</z>\n\
      <height>532</height>\n\
      <location_x>0</location_x>\n\
      <location_y>301</location_y>\n\
      </plugin>\n\
      <plugin>\n\
      se.sics.cooja.plugins.TimeLine\n\
      <plugin_config>\n\
      <showRadioRXTX />\n\
      <showRadioHW />\n\
      <showLEDs />\n\
      <split>125</split>\n\
      <zoomfactor>500.0</zoomfactor>\n\
      </plugin_config>\n\
      <width>1680</width>\n\
      <z>4</z>\n\
      <height>150</height>\n\
      <location_x>0</location_x>\n\
      <location_y>833</location_y>\n\
      </plugin>\n");
printScript(timeout);
}


