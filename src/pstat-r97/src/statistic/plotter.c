#include "../settings.h"
#include "plotter.h"
#include <math.h>


void vgenerate_ggraph_hist(const char *graph_type, const char *datain_filename, 
	const char *dataout_type, const char *dataout_filename, const char *title, 
	const char *xlabel, const char *ylabel, int xcolumn, int ycolumn, 
	int bitmap_width, int bitmap_height)
{
	
	FILE *gnuplot = NULL;
	
	gnuplot = popen(GNUPLOT_PATH, "w");
	if(gnuplot == NULL)
	{
		fprintf(stderr, "%s not found. \n", GNUPLOT_PATH);
		exit(EXIT_FAILURE);
	}
	
/* building an absolute path is here needed to use popen */
	/*create the file for the output if doesn't exist */
	FILE *dataout;
	dataout = fopen(dataout_filename, "w");
	
	if(dataout == NULL)
	{
		fprintf(stderr, "Impossible to open/create %s. \n", dataout_filename);
		exit(EXIT_FAILURE);
	}
	
	fclose(dataout);
	 
	char path_datain_filename[PATH_MAX+1];
	char path_dataout_filename[PATH_MAX+1];
	char *returned_path_datain_filename;
	char *returned_path_dataout_filename;
	
	/* comment : realpath returns a pointer on the string of the absolute path, 
	if the call succeed. If the second argument (path_datain_filename or 
	path_pngout_filename) is setted at NULL by realpath, then realpath uses 
	malloc. See the man documentation for more information. */
	returned_path_datain_filename = realpath(datain_filename, path_datain_filename);
	returned_path_dataout_filename = realpath(dataout_filename, path_dataout_filename);
	
	if(returned_path_datain_filename == NULL)
	{
		fprintf(stderr, "%s not found. \n", datain_filename);
		exit(EXIT_FAILURE);
	}
	
	if(returned_path_dataout_filename == NULL)
	{
		fprintf(stderr, "%s not found. \n", dataout_filename);
		exit(EXIT_FAILURE);
	}


/* gnuplot execution */
	if (strcmp(graph_type, "INDIV") == 0) /* if graph_type == "INDIV" */
	{
		if (strcmp(dataout_type, "PNG") == 0)
			fprintf(gnuplot, "set terminal png size %d, %d\n", bitmap_width, bitmap_height);
		else if(strcmp(dataout_type, "EPS") == 0)
			fprintf(gnuplot, "set terminal post eps\n");
		else if(strcmp(dataout_type, "PDF") == 0)
			fprintf(gnuplot, "set terminal pdf\n");
		
/*		fprintf(gnuplot, "set style histogram\n");*/
		fprintf(gnuplot, "set style data histogram\n");
		fprintf(gnuplot, "set xtics rotate by -45\n");
		fprintf(gnuplot, "set output '%s'\n", returned_path_dataout_filename);
		fprintf(gnuplot, "set xlabel '%s'\n", xlabel);
		fprintf(gnuplot, "set ylabel '%s'\n", ylabel);
		fprintf(gnuplot, "set title '%s'\n", title);
		fprintf(gnuplot, "plot '%s' using %d:xtic(%d) title ''\n", returned_path_datain_filename, xcolumn, ycolumn);
	}
	else if (strcmp(graph_type, "STAT") == 0)
	{
		if (strcmp(dataout_type, "PNG") == 0)
			fprintf(gnuplot, "set terminal png size %d, %d\n", bitmap_width, bitmap_height);
		else if(strcmp(dataout_type, "EPS") == 0)
			fprintf(gnuplot, "set terminal post eps\n");
		else if(strcmp(dataout_type, "PDF") == 0)
			fprintf(gnuplot, "set terminal pdf\n");
		
/*		fprintf(gnuplot, "set style histogram\n");*/
		fprintf(gnuplot, "set style data histogram\n");
		fprintf(gnuplot, "set output '%s'\n", returned_path_dataout_filename);
		fprintf(gnuplot, "set xlabel 'Individuals'\n");
		fprintf(gnuplot, "set ylabel '%s'\n", ylabel);
		fprintf(gnuplot, "set title '%s'\n", title); 
		fprintf(gnuplot, "plot '%s' using %d title ''\n", returned_path_datain_filename, ycolumn);
	}
	
	fflush(gnuplot);
	pclose(gnuplot);
	
	if(path_datain_filename == NULL)
		free(returned_path_datain_filename);
	
	if(path_dataout_filename == NULL)
		free(returned_path_dataout_filename);
	
}



void vgenerate_ggraph(const char *graph_type, const char *datain_filename, 
	const char *dataout_type, const char *dataout_filename, const char *title, 
	const char *xlabel, const char *ylabel, int xcolumn, int ycolumn, 
	int bitmap_width, int bitmap_height)
{
	
	FILE *gnuplot = NULL;
	
	gnuplot = popen(GNUPLOT_PATH, "w");
	if(gnuplot == NULL)
	{
		fprintf(stderr, "%s not found. \n", GNUPLOT_PATH);
		exit(EXIT_FAILURE);
	}
	
/* building an absolute path is here needed to use popen */
	/*create the file for the output if doesn't exist */
	FILE *dataout;
	dataout = fopen(dataout_filename, "w");
	
	if(dataout == NULL)
	{
		fprintf(stderr, "Impossible to open/create %s. \n", dataout_filename);
		exit(EXIT_FAILURE);
	}
	
	fclose(dataout);
	 
	char path_datain_filename[PATH_MAX+1];
	char path_dataout_filename[PATH_MAX+1];
	char *returned_path_datain_filename;
	char *returned_path_dataout_filename;
	
	/* comment : realpath returns a pointer on the string of the absolute path, 
	if the call succeed. If the second argument (path_datain_filename or 
	path_pngout_filename) is setted at NULL by realpath, then realpath uses 
	malloc. See the man documentation for more information. */
	returned_path_datain_filename = realpath(datain_filename, path_datain_filename);
	returned_path_dataout_filename = realpath(dataout_filename, path_dataout_filename);
	
	if(returned_path_datain_filename == NULL)
	{
		fprintf(stderr, "%s not found. \n", datain_filename);
		exit(EXIT_FAILURE);
	}
	
	if(returned_path_dataout_filename == NULL)
	{
		fprintf(stderr, "%s not found. \n", dataout_filename);
		exit(EXIT_FAILURE);
	}


/* gnuplot execution */
	if (strcmp(graph_type, "INDIV") == 0) /* if graph_type == "INDIV" */
	{
		if (strcmp(dataout_type, "PNG") == 0)
			fprintf(gnuplot, "set terminal png size %d, %d\n", bitmap_width, bitmap_height);
		else if(strcmp(dataout_type, "EPS") == 0)
			fprintf(gnuplot, "set terminal post eps\n");
		else if(strcmp(dataout_type, "PDF") == 0)
			fprintf(gnuplot, "set terminal pdf\n");
		
		fprintf(gnuplot, "set output '%s'\n", returned_path_dataout_filename);
		fprintf(gnuplot, "set xlabel '%s'\n", xlabel);
		fprintf(gnuplot, "set ylabel '%s'\n", ylabel);
		fprintf(gnuplot, "set title '%s'\n", title); 
		fprintf(gnuplot, "plot '%s' using %d:%d title ''\n", returned_path_datain_filename, xcolumn, ycolumn);
	}
	else if (strcmp(graph_type, "STAT") == 0)
	{
		if (strcmp(dataout_type, "PNG") == 0)
			fprintf(gnuplot, "set terminal png size %d, %d\n", bitmap_width, bitmap_height);
		else if(strcmp(dataout_type, "EPS") == 0)
			fprintf(gnuplot, "set terminal post eps\n");
		else if(strcmp(dataout_type, "PDF") == 0)
			fprintf(gnuplot, "set terminal pdf\n");
		
		fprintf(gnuplot, "set output '%s'\n", returned_path_dataout_filename);
		fprintf(gnuplot, "set xlabel 'Individuals'\n");
		fprintf(gnuplot, "set ylabel '%s'\n", ylabel);
		fprintf(gnuplot, "set title '%s'\n", title); 
		fprintf(gnuplot, "plot '%s' using %d title ''\n", returned_path_datain_filename, ycolumn);
	}
	
	fflush(gnuplot);
	pclose(gnuplot);
	
	if(path_datain_filename == NULL)
		free(returned_path_datain_filename);
	
	if(path_dataout_filename == NULL)
		free(returned_path_dataout_filename);
	
}


void generate_ggraph(const char* graph_type, const char* datain_filename, 
	const char* dataout_type, const char* dataout_filename, 
	const char* title, ...)
{
	
	va_list args;
	va_start(args, title);
	
	if (strcmp(graph_type, "INDIV") == 0) /* if graph_type == "INDIV" */
	{
		if (strcmp(dataout_type, "PNG") == 0)
		{
			char* xlabel=va_arg(args, char*);
			char* ylabel=va_arg(args, char*);
			int xcolumn=va_arg(args, int);
			int ycolumn=va_arg(args, int);
			int bitmap_width=va_arg(args, int);
			int bitmap_height=va_arg(args, int);
			
			vgenerate_ggraph(graph_type, datain_filename, 
				dataout_type, dataout_filename, title,
				xlabel, ylabel, xcolumn, ycolumn, 
				bitmap_width, bitmap_height);
		}
		else if(strcmp(dataout_type, "EPS") == 0)
		{
			char* xlabel=va_arg(args, char*);
			char* ylabel=va_arg(args, char*);
			int xcolumn=va_arg(args, int);
			int ycolumn=va_arg(args, int);
			
			vgenerate_ggraph(graph_type, datain_filename, 
				dataout_type, dataout_filename, title,
				xlabel, ylabel, xcolumn, ycolumn, 
				0, 0);
		}
		else if(strcmp(dataout_type, "PDF") == 0)
		{
			char* xlabel=va_arg(args, char*);
			char* ylabel=va_arg(args, char*);
			int xcolumn=va_arg(args, int);
			int ycolumn=va_arg(args, int);
			
			vgenerate_ggraph(graph_type, datain_filename, 
				dataout_type, dataout_filename, title,
				xlabel, ylabel, xcolumn, ycolumn, 
				0, 0);
		}
	}
	else if (strcmp(graph_type, "STAT") == 0)
	{
		if (strcmp(dataout_type, "PNG") == 0)
		{
			char* ylabel=va_arg(args, char*);
			int column=va_arg(args, int);
			int bitmap_width=va_arg(args, int);
			int bitmap_height=va_arg(args, int);
			
			vgenerate_ggraph(graph_type, datain_filename, 
				dataout_type, dataout_filename, title,
				NULL, ylabel, 0, column, 
				bitmap_width, bitmap_height);
		}
		else if(strcmp(dataout_type, "EPS") == 0)
		{
			char* ylabel=va_arg(args, char*);
			int column=va_arg(args, int);
			
			vgenerate_ggraph(graph_type, datain_filename, 
				dataout_type, dataout_filename, title,
				NULL, ylabel, 0, column, 
				0, 0);
		}
		else if(strcmp(dataout_type, "PDF") == 0)
		{
			char* ylabel=va_arg(args, char*);
			int column=va_arg(args, int);
			
			vgenerate_ggraph(graph_type, datain_filename, 
				dataout_type, dataout_filename, title,
				NULL, ylabel, 0, column, 
				0, 0);
		}
	}
	else
	{
		fprintf(stderr, "Error: Function 'generate_graph' missused. Invalid arguments. \n");
		exit(EXIT_FAILURE);
	}
	
	va_end(args);
	
}




void init_gnuplot(const char *pngout_filename, int pngout_width, int pngout_height)
{
	
	FILE *gnuplot = NULL;
	
	gnuplot = popen(GNUPLOT_PATH, "w");
	if(gnuplot == NULL)
	{
		fprintf(stderr, "%s not found. \n", GNUPLOT_PATH);
		exit(EXIT_FAILURE);
	}
	
/* building an absolute path is here needed to use popen */
	/*create the file for the output if doesn't exist */
	FILE *pngout;
	pngout = fopen(pngout_filename, "w");
	
	if(pngout == NULL)
	{
		fprintf(stderr, "Impossible to open/create %s. \n", pngout_filename);
		exit(EXIT_FAILURE);
	}
	
	fclose(pngout);
	 
	char path_pngout_filename[PATH_MAX+1];
	char *returned_path_pngout_filename;
	
	/* comment : realpath returns a pointer on the string of the absolute path, 
	if the call succeed. If the second argument (path_datain_filename or 
	path_pngout_filename) is setted at NULL by realpath, then realpath uses 
	malloc. See the man documentation for more information. */
	returned_path_pngout_filename = realpath(pngout_filename, path_pngout_filename);
	
	if(returned_path_pngout_filename == NULL)
	{
		fprintf(stderr, "%s not found. \n", pngout_filename);
		exit(EXIT_FAILURE);
	}

/* gnuplot execution */	
	fprintf(gnuplot, "set terminal png size %d, %d\n", pngout_width, pngout_height);
	fprintf(gnuplot, "set output '%s'\n", returned_path_pngout_filename);
	fprintf(gnuplot, "set xlabel 'x'\n");
	fprintf(gnuplot, "set ylabel 'y'\n");
	/* fprintf(gnuplot, "set title 'coucou'\n"); */
	fprintf(gnuplot, "plot [0:10][0:10] 0 notitle\n");
	fflush(gnuplot);
	pclose(gnuplot);
	
	if(path_pngout_filename == NULL)
		free(returned_path_pngout_filename);
	
}


static int comparstr(const void *string1, const void *string2)
{
	return strcmp(* (char * const *) string1, * (char * const *) string2);
}

/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* version d'entries-static.c (cf sous-projet [entries_dir]): allocs statiques*/
/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
char** list_files(const char* folder, const char* filter)
{
	char** entries = NULL;
	
	DIR *directory;
	struct dirent *entry;
	
	directory = opendir(folder);
	
	if (directory == NULL)
	{
/*		perror ("Couldn't open the directory");*/
		fprintf(stderr, "The folder %s cannot be opened. \n", folder);
		exit(EXIT_FAILURE);
	}
	
	
	int entry_name_size = 0;
	int index_entries = 0;
	
	entries = calloc(1000, sizeof(char*));
	*(entries + 999) = NULL; /* the last string pointer is always at NULL */
	
	do {
		entry = readdir(directory);
	} while(entry && !strstr(entry->d_name, filter));
	
	while (entry) /* while entry != NULL */
	{
		entry_name_size = strlen(entry->d_name);
		
		*(entries + index_entries) = malloc(1000*sizeof(char));
		*(*(entries + index_entries)) = '\0';
		
		strcat(*(entries + index_entries), entry->d_name);
		*(*(entries + index_entries) + entry_name_size) = '\0';
		
		index_entries++;
		do {
			entry = readdir(directory);
		} while(entry && !strstr(entry->d_name, filter));
	}
	closedir(directory);
	index_entries--;
	
	/* sort entries according to ascii table */
	qsort(entries, index_entries+1, sizeof(char*), comparstr);
	
	return entries;
}


void pdf(const char *datain_filename, int column)
{
	FILE *datain;
	datain = fopen(datain_filename, "r");
	
	if(datain == NULL)
	{
		fprintf(stderr, "Cannot open %s. \n", datain_filename);
		exit(EXIT_FAILURE);
	}
	
	char *s=malloc(10000*sizeof(char));
	int sample_size=0;
	
	fgets(s, 9999, datain);
	while (!feof(datain))
	{
		fgets(s, 9999, datain);
		
		if (!feof(datain))
			sample_size++;
	}
	fclose(datain);
	
	printf("QQQQ : %d\n", sample_size);
	
	
	
	FILE *gnuplot = NULL;
	
	gnuplot = popen(GNUPLOT_PATH, "w");
	if(gnuplot == NULL)
	{
		fprintf(stderr, "%s not found. \n", GNUPLOT_PATH);
		exit(EXIT_FAILURE);
	}
	
/* building an absolute path is here needed to use popen */
	/*create the file for the output if doesn't exist */
	FILE *pngout;
	pngout = fopen(DATA_OUT "test.png", "w");
	
	if(pngout == NULL)
	{
		fprintf(stderr, "Impossible to open/create %s. \n", DATA_OUT "test.png");
		exit(EXIT_FAILURE);
	}
	
	fclose(pngout);
	 
	char path_pngout_filename[PATH_MAX+1];
	char *returned_path_pngout_filename;
	
	/* comment : realpath returns a pointer on the string of the absolute path, 
	if the call succeed. If the second argument (path_datain_filename or 
	path_pngout_filename) is setted at NULL by realpath, then realpath uses 
	malloc. See the man documentation for more information. */
	returned_path_pngout_filename = realpath(DATA_OUT "test.png", path_pngout_filename);
	
	if(returned_path_pngout_filename == NULL)
	{
		fprintf(stderr, "%s not found. \n", DATA_OUT "test.png");
		exit(EXIT_FAILURE);
	}

/* gnuplot execution */	
	fprintf(gnuplot, "set terminal png size 640, 480\n");
	fprintf(gnuplot, "set output '%s'\n", returned_path_pngout_filename);
	fprintf(gnuplot, "set xlabel 'x'\n");
	fprintf(gnuplot, "set ylabel 'y'\n");
	/* fprintf(gnuplot, "set title 'coucou'\n"); */
	fprintf(gnuplot, "plot '%s' using 2:(1./%f) smooth frequency\n", datain_filename, (float)sample_size);
	fflush(gnuplot);
	pclose(gnuplot);
	
	if(path_pngout_filename == NULL)
		free(returned_path_pngout_filename);
	
}


void pdff()
{
	FILE *datain;
	datain = fopen(DATA_OUT "dataoutmean.dat", "r");
	if(datain == NULL)
	{
		fprintf(stderr, "Cannot open %s. \n", DATA_OUT "dataoutmean.dat");
		exit(EXIT_FAILURE);
	}
	
	FILE *dataout;
	dataout = fopen(DATA_OUT "dataout.dat", "w+");
	if(dataout == NULL)
	{
		fprintf(stderr, "Cannot open %s. \n", DATA_OUT "dataout.dat");
		exit(EXIT_FAILURE);
	}
	fclose(dataout);
	
	FILE *datatemp;
	
	
	char *s=malloc(10000*sizeof(char));
	
	float sample_size=0.0;
	fgets(s, 9999, datain);
	while (!feof(datain))
	{
		fgets(s, 9999, datain);
		
		if (!feof(datain))
			sample_size++;
	}
	rewind(datain);
	printf("samplesize dataoutmean.dat: %f\n", sample_size);
	
	
	float value, valout, freq, count;
	valout=NAN;
	
	while (!feof(datain))
	{
		fgets(s, 9999, datain);
		fscanf(datain, "%f", &value);
		printf("cur datain : %f\n", value);
		
		if (!feof(datain))
		{
			dataout = fopen(DATA_OUT "dataout.dat", "r");
			if(dataout == NULL)
			{
				fprintf(stderr, "Cannot open %s. \n", DATA_OUT "dataout.dat");
				exit(EXIT_FAILURE);
			}
			
			datatemp = fopen(DATA_OUT "datatemp.dat", "w+");
			if(datatemp == NULL)
			{
				fprintf(stderr, "Cannot open %s. \n", DATA_OUT "datatemp.dat");
				exit(EXIT_FAILURE);
			}
			fprintf(datatemp, "# means	PDF	sums\n");
			
			fgets(s, 9999, dataout);
			fscanf(dataout, "%f	%f	%f", &valout, &freq, &count);
			while (!feof(dataout) && valout<value)
			{
				fprintf(datatemp, "%f	%f	%f\n", valout, freq, count);
				fgets(s, 9999, dataout);
				fscanf(dataout, "%f	%f	%f", &valout, &freq, &count);
			}
			if(feof(dataout))
			{
				fprintf(datatemp, "%f	%f	%f\n", value, (1.0/sample_size), 1.0);
				printf("cur valout if : %f\n", valout);
			}
			else if (valout==value)
			{
				count = count+1.0;
				freq = count/sample_size;
				fprintf(datatemp, "%f	%f	%f\n", valout, freq, count);
				printf("cur valout else : %f, %f\n", valout, count);
				fgets(s, 9999, dataout);
				fscanf(dataout, "%f	%f	%f", &valout, &freq, &count);
				while (!feof(dataout))
				{
					fprintf(datatemp, "%f	%f	%f\n", valout, freq, count);
					fgets(s, 9999, dataout);
					fscanf(dataout, "%f	%f	%f", &valout, &freq, &count);
				}
			}
			else
			{
				fprintf(datatemp, "%f	%f	%f\n", value, (1.0/sample_size), 1.0);
				printf("cur valout if : %f\n", valout);
				while (!feof(dataout))
				{
					fprintf(datatemp, "%f	%f	%f\n", valout, freq, count);
					fgets(s, 9999, dataout);
					fscanf(dataout, "%f	%f	%f", &valout, &freq, &count);
				}
			}
			fclose(dataout);
			remove(DATA_OUT "dataout.dat");
			fclose(datatemp);
			rename(DATA_OUT "datatemp.dat", DATA_OUT  "dataout.dat");
			remove(DATA_OUT "datatemp.dat");
		}
	}




	
	fclose(datain);
	
}



