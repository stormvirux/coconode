#include "settings.h"
#include <stdlib.h>
#include <gtk/gtk.h>
#include "gui/gui_actions.h"
#include "statistic/plotter.h"


int main(int argc, char *argv [])
{
/*pdf(DATA_IN "data.dat", 2);*/
/*pdff();*/

/* declarations of the GTK widgets following a tree model */
	GtkWidget *main_window = NULL;
		GtkWidget *main_scrolled_window = NULL;
			GtkWidget *vbox_main = NULL;
				GtkWidget *hbox_global_settings_status = NULL;
					GtkWidget *label_xaxis = NULL;
					GtkWidget *combo_xaxis = NULL;
					GtkWidget *label_yaxis = NULL;
					GtkWidget *combo_yaxis = NULL;
				GtkWidget *hbox_graphs = NULL;
					GtkWidget *vbox_one_individual = NULL;
						GtkWidget *hbox_one_individual_settings = NULL;
							GtkWidget *label_one_individual = NULL;
							GtkWidget *combo_one_individual = NULL;
							GtkWidget *button_one_individual_trace = NULL;
						GtkWidget *graph_one_individual = NULL;
						GtkWidget *hbox_one_individual_export = NULL;
							GtkWidget *label_one_individual_format = NULL;
							GtkWidget *combo_one_individual_format = NULL;
							GtkWidget *button_one_individual_export = NULL;
					GtkWidget *vbox_stat = NULL;
						GtkWidget *hbox_stat_settings = NULL;
							GtkWidget *label_stat_function = NULL;
							GtkWidget *combo_stat_function = NULL;
							GtkWidget *button_stat_compute = NULL;
						GtkWidget *graph_stat = NULL;
						GtkWidget *hbox_stat_export = NULL;
							GtkWidget *label_stat_format = NULL;
							GtkWidget *combo_stat_format = NULL;
							GtkWidget *button_stat_export = NULL;
	
	GdkPixbuf *pixbuf_graph_one_individual = NULL;
	GdkPixbuf *pixbuf_graph_stat = NULL;


/* initialization of the input file list and of the individual traced 
	at start up (the first of the list) */
	char** entries = list_files(DATA_IN, FILENAME_FILTER);
/*	char* oneindiv_traced = calloc(strlen(*(entries))+1, sizeof(char));*/
	char* oneindiv_traced = calloc(2000, sizeof(char));
	strcpy(oneindiv_traced, *(entries));
	printf("Default file firstly traced used to determine the differents axis of individuals : %s\n", oneindiv_traced);
	
	if(oneindiv_traced == NULL)
	{
		fprintf(stderr, "No input file found! \n");
		exit(EXIT_FAILURE);
	}


/* initialization of GTK */
	gtk_disable_setlocale();
	gtk_init(&argc, &argv);


/* main window */
	main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	gtk_window_set_title(GTK_WINDOW(main_window), "Coconode - plotter v2.0");
/*	gtk_window_set_position(GTK_WINDOW(main_window), GTK_WIN_POS_CENTER);*/
	gtk_window_move(GTK_WINDOW(main_window), 0, 0); 
	gtk_window_set_default_size(GTK_WINDOW(main_window), MAIN_WINDOW_DEFAULT_WIDTH, MAIN_WINDOW_DEFAULT_HEIGHT); 
/*	gtk_window_maximize(GTK_WINDOW(main_window)); */
/*	gtk_window_iconify(GTK_WINDOW(main_window)); */
/*	gtk_window_deiconify(GTK_WINDOW(main_window)); */
/*	gtk_widget_set_usize(main_window, 640, 480);*/ /* set a minimum size */
	
	/* set the application icon */
	const gchar *icon = "./media/icon-small.png\0";
	gtk_window_set_icon_from_file(GTK_WINDOW(main_window), icon, NULL);
	
	/* set the close button fuction of the main window to gtk_main_quit() */
	/* g_signal_connect(G_OBJECT(main_window), "destroy", (GCallback)gtk_main_quit, NULL); */
	g_signal_connect(G_OBJECT(main_window), "delete-event", G_CALLBACK(quit), NULL);
	
/*	gtk_widget_show(main_window); */


/* main scrolled window */
	main_scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(main_scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(main_window), main_scrolled_window);


/* vbox_main */
						/* homogeneous, spacing */
	vbox_main = gtk_vbox_new(FALSE, 0);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(main_scrolled_window), vbox_main);


/* hbox_global_settings_status */
											/* homogeneous, spacing */
	hbox_global_settings_status = gtk_hbox_new(FALSE, 0);
														/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(vbox_main), hbox_global_settings_status, TRUE, FALSE, 0);


/* label_xaxis */
	label_xaxis = gtk_label_new("x axis: ");
	gtk_box_pack_start(GTK_BOX(hbox_global_settings_status), label_xaxis, FALSE, FALSE, 0);


/* combo_xaxis */
char* datain_filename_relpath = malloc(1000*sizeof(char));
sprintf(datain_filename_relpath, "%s%s", DATA_IN, oneindiv_traced);
	combo_xaxis = gtk_combo_box_new_text();
	add_combo_box_entries_from_datain(combo_xaxis, datain_filename_relpath);
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo_xaxis), 0);
	/* gtk_combo_box_append_text(GTK_COMBO_BOX(combo_xaxis), "test_text");*/
	gtk_box_pack_start(GTK_BOX(hbox_global_settings_status), combo_xaxis, FALSE, FALSE, 0);


/* label_yaxis */
	label_yaxis = gtk_label_new("y axis: ");
	gtk_box_pack_start(GTK_BOX(hbox_global_settings_status), label_yaxis, FALSE, FALSE, 0);


/* combo_yaxis */
	combo_yaxis = gtk_combo_box_new_text();
	add_combo_box_entries_from_datain(combo_yaxis, datain_filename_relpath);
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo_yaxis), 1);
	gtk_box_pack_start(GTK_BOX(hbox_global_settings_status), combo_yaxis, FALSE, FALSE, 0);
free(datain_filename_relpath);


/* hbox_graphs */
							/* homogeneous, spacing */
	hbox_graphs = gtk_hbox_new(FALSE, 0);
										/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(vbox_main), hbox_graphs, TRUE, FALSE, 0);


/* vbox_one_individual */
									/* homogeneous, spacing */
	vbox_one_individual = gtk_vbox_new(FALSE, 0);
												/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(hbox_graphs), vbox_one_individual, TRUE, FALSE, 0);


/* hbox_one_individual_settings */
										/* homogeneous, spacing */
	hbox_one_individual_settings = gtk_hbox_new(FALSE, 0);
												/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(vbox_one_individual), hbox_one_individual_settings, TRUE, FALSE, 0);


/* label_one_individual */
	label_one_individual = gtk_label_new("current individual: ");
	gtk_box_pack_start(GTK_BOX(hbox_one_individual_settings), label_one_individual, FALSE, FALSE, 0);


/* combo_one_individual */
	combo_one_individual = gtk_combo_box_new_text();
	add_combo_box_entries_from_list_files(combo_one_individual);
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo_one_individual), 0);
/*	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_one_individual), "test_text");*/
	gtk_box_pack_start(GTK_BOX(hbox_one_individual_settings), combo_one_individual, FALSE, FALSE, 0);


/* button_one_individual_trace */
	button_one_individual_trace = gtk_button_new_with_label("trace");
	gtk_box_pack_start(GTK_BOX(hbox_one_individual_settings), button_one_individual_trace, FALSE, FALSE, 0);


/* graph_one_individual */
	graph_one_individual = gtk_image_new();
	
	init_gnuplot(DATA_DISPLAYED "/oneindiv.png", MAIN_WINDOW_DEFAULT_WIDTH, MAIN_WINDOW_DEFAULT_HEIGHT);
	pixbuf_graph_one_individual = gdk_pixbuf_new_from_file(DATA_DISPLAYED "/oneindiv.png", NULL);
	gtk_image_set_from_pixbuf(GTK_IMAGE(graph_one_individual), pixbuf_graph_one_individual);
	
	gtk_box_pack_start(GTK_BOX(vbox_one_individual), graph_one_individual, FALSE, FALSE, 0);
	
																				/* width, height, error */
	/*	pixbuf_graph_one_individual = gdk_pixbuf_new_from_file_at_size("DATA_DISPLAYED/oneindiv.png", 640, 480, NULL);*/
	/*GdkPixbuf* gdk_pixbuf_scale_simple(const GdkPixbuf *src, int dest_width, int dest_height, GdkInterpType interp_type);*/
	/*	pixbuf_graph_one_individual = gdk_pixbuf_scale_simple(pixbuf_graph_one_individual, 640, 480, GDK_INTERP_BILINEAR);*/
	
	/*GtkWidget* gtk_image_new_from_pixbuf(GdkPixbuf *pixbuf);*/
	/*GdkPixbuf* gtk_image_get_pixbuf(GtkImage *image);*/
	
	/*	graph_one_individual = gtk_image_new_from_pixbuf(pixbuf_graph_one_individual);*/

	/*const GdkPixbuf *pixbuf_graph_one_individual = gtk_image_get_pixbuf(GTK_IMAGE(graph_one_individual));*/
	/*printf("image is %ux%u pixels\n", gdk_pixbuf_get_width(pixbuf_graph_one_individual), gdk_pixbuf_get_height(pixbuf_graph_one_individual));*/


/* hbox_one_individual_export */
										/* homogeneous, spacing */
	hbox_one_individual_export = gtk_hbox_new(FALSE, 0);
												/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(vbox_one_individual), hbox_one_individual_export, TRUE, FALSE, 0);


/* label_one_individual_format */
	label_one_individual_format = gtk_label_new("export format: ");
	gtk_box_pack_start(GTK_BOX(hbox_one_individual_export), label_one_individual_format, FALSE, FALSE, 0);


/* combo_one_individual_format */
	combo_one_individual_format = gtk_combo_box_new_text();
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_one_individual_format), "EPS");
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_one_individual_format), "PDF");
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_one_individual_format), "PNG");
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo_one_individual_format), 0);
	gtk_box_pack_start(GTK_BOX(hbox_one_individual_export), combo_one_individual_format, FALSE, FALSE, 0);


/* button_one_individual_export */
	button_one_individual_export = gtk_button_new_with_label("export");
	gtk_box_pack_start(GTK_BOX(hbox_one_individual_export), button_one_individual_export, FALSE, FALSE, 0);


/* vbox_stat */
									/* homogeneous, spacing */
	vbox_stat = gtk_vbox_new(FALSE, 0);
												/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(hbox_graphs), vbox_stat, TRUE, FALSE, 0);


/* hbox_stat_settings */
										/* homogeneous, spacing */
	hbox_stat_settings = gtk_hbox_new(FALSE, 0);
												/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(vbox_stat), hbox_stat_settings, TRUE, FALSE, 0);


/* label_stat_function */
	label_stat_function = gtk_label_new("statistic function: ");
	gtk_box_pack_start(GTK_BOX(hbox_stat_settings), label_stat_function, FALSE, FALSE, 0);


/* combo_stat_function */
	combo_stat_function = gtk_combo_box_new_text();
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_stat_function), "mean");
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_stat_function), "probability density function on mean");
/*	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_stat_function), "cumulative density function");*/
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stat_function), 0);
	gtk_box_pack_start(GTK_BOX(hbox_stat_settings), combo_stat_function, FALSE, FALSE, 0);


/* button_stat_compute */
	button_stat_compute = gtk_button_new_with_label("compute");
	gtk_box_pack_start(GTK_BOX(hbox_stat_settings), button_stat_compute, FALSE, FALSE, 0);


/* graph_stat */
	graph_stat = gtk_image_new();
	
	init_gnuplot(DATA_DISPLAYED "/stat.png", MAIN_WINDOW_DEFAULT_WIDTH, MAIN_WINDOW_DEFAULT_HEIGHT);
	pixbuf_graph_stat = gdk_pixbuf_new_from_file(DATA_DISPLAYED "/stat.png", NULL);
	gtk_image_set_from_pixbuf(GTK_IMAGE(graph_stat), pixbuf_graph_stat);
	
	gtk_box_pack_start(GTK_BOX(vbox_stat), graph_stat, FALSE, FALSE, 0);
	
																				/* width, height, error */
	/*	pixbuf_graph_stat = gdk_pixbuf_new_from_file_at_size("DATA_DISPLAYED/stat.png", 640, 480, NULL);*/
	/*GdkPixbuf* gdk_pixbuf_scale_simple(const GdkPixbuf *src, int dest_width, int dest_height, GdkInterpType interp_type);*/
	/*	pixbuf_graph_stat = gdk_pixbuf_scale_simple(pixbuf_graph_stat, 640, 480, GDK_INTERP_BILINEAR);*/
	
	/*GtkWidget* gtk_image_new_from_pixbuf(GdkPixbuf *pixbuf);*/
	/*GdkPixbuf* gtk_image_get_pixbuf(GtkImage *image);*/
	
	/*	graph_stat = gtk_image_new_from_pixbuf(pixbuf_graph_stat);*/

	/*const GdkPixbuf *pixbuf_graph_stat = gtk_image_get_pixbuf(GTK_IMAGE(graph_stat));*/
	/*printf("image is %ux%u pixels\n", gdk_pixbuf_get_width(pixbuf_graph_stat), gdk_pixbuf_get_height(pixbuf_graph_stat));*/


/* hbox_stat_export */
								/* homogeneous, spacing */
	hbox_stat_export = gtk_hbox_new(FALSE, 0);
										/* expand, fill, padding (=spacing) */
	gtk_box_pack_start(GTK_BOX(vbox_stat), hbox_stat_export, TRUE, FALSE, 0);


/* label_stat_format */
	label_stat_format = gtk_label_new("export format: ");
	gtk_box_pack_start(GTK_BOX(hbox_stat_export), label_stat_format, FALSE, FALSE, 0);


/* combo_stat_format */
	combo_stat_format = gtk_combo_box_new_text();
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_stat_format), "EPS");
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_stat_format), "PDF");
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_stat_format), "PNG");
	gtk_combo_box_set_active(GTK_COMBO_BOX(combo_stat_format), 0);
	gtk_box_pack_start(GTK_BOX(hbox_stat_export), combo_stat_format, FALSE, FALSE, 0);


/* button_stat_export */
	button_stat_export = gtk_button_new_with_label("export");
	gtk_box_pack_start(GTK_BOX(hbox_stat_export), button_stat_export, FALSE, FALSE, 0);


/* show all */
	gtk_widget_show_all(main_window);


/* callbacks except 'quit' */
	GenerateGraphWidgets gene_graph_widgs;
	gene_graph_widgs.main_window = main_window;
	gene_graph_widgs.graph_one_individual = graph_one_individual;
	gene_graph_widgs.pixbuf_graph_one_individual = pixbuf_graph_one_individual;
	gene_graph_widgs.graph_stat = graph_stat;
	gene_graph_widgs.pixbuf_graph_stat = pixbuf_graph_stat;
	
	gene_graph_widgs.hbox_global_settings_status = hbox_global_settings_status;
	gene_graph_widgs.hbox_one_individual_settings = hbox_one_individual_settings;
	gene_graph_widgs.hbox_one_individual_export = hbox_one_individual_export;
	gene_graph_widgs.hbox_stat_settings = hbox_stat_settings;
	gene_graph_widgs.hbox_stat_export = hbox_stat_export;
	
	gene_graph_widgs.oneindiv_traced = oneindiv_traced;
	gene_graph_widgs.combo_stat_function = combo_stat_function;
	gene_graph_widgs.combo_xaxis = combo_xaxis;
	gene_graph_widgs.combo_yaxis = combo_yaxis;
	
/*	generate_graph(button_one_individual_trace, (gpointer) &gene_graph_widgs);*/
	g_signal_connect(main_window, "check-resize", G_CALLBACK(generate_graph), &gene_graph_widgs);
	
	TraceOneIndivGraphWidgets tr_oneindiv_graph;
	tr_oneindiv_graph.oneindiv_traced = oneindiv_traced;
	tr_oneindiv_graph.combo_one_individual = combo_one_individual;
	tr_oneindiv_graph.gene_graph_widgs = &gene_graph_widgs;
	g_signal_connect(G_OBJECT(button_one_individual_trace), "clicked", G_CALLBACK(trace_oneindiv_graph), &tr_oneindiv_graph);
	
	
	CompStatGraphWidgets comp_stat_graph;
	comp_stat_graph.combo_yaxis = combo_yaxis;
	comp_stat_graph.combo_stat_function = combo_stat_function;
	comp_stat_graph.gene_graph_widgs = &gene_graph_widgs;
	g_signal_connect(G_OBJECT(button_stat_compute), "clicked", G_CALLBACK(compute_stat_graph), &comp_stat_graph);
	
	/*premier calcul pour le lancement de l'application*/
	compute_stat_graph(button_stat_compute, &comp_stat_graph);
	
	
	ExportGraphOneIndivWidgets exp_oneindiv_widgs;
	exp_oneindiv_widgs.main_window = main_window; 
	exp_oneindiv_widgs.combo_one_individual_format = combo_one_individual_format;
	exp_oneindiv_widgs.combo_one_individual = combo_one_individual;
	exp_oneindiv_widgs.combo_xaxis = combo_xaxis;
	exp_oneindiv_widgs.combo_yaxis = combo_yaxis;
	g_signal_connect(G_OBJECT(button_one_individual_export), "clicked", G_CALLBACK(export_graph_one_indiv), &exp_oneindiv_widgs);
	
	
	ExportGraphStatWidgets exp_stat_widgs;
	exp_stat_widgs.main_window = main_window; 
	exp_stat_widgs.combo_stat_format = combo_stat_format;
	exp_stat_widgs.combo_stat_function = combo_stat_function;
	exp_stat_widgs.combo_xaxis = combo_xaxis;
	exp_stat_widgs.combo_yaxis = combo_yaxis;
	g_signal_connect(G_OBJECT(button_stat_export), "clicked", G_CALLBACK(export_graph_stat), &exp_stat_widgs);
	
	
	g_signal_connect(G_OBJECT(combo_one_individual), "set-focus-child", G_CALLBACK(update_combo_oneindiv), NULL);
	
	gtk_main();
	
	return EXIT_SUCCESS;
}

